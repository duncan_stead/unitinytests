
namespace game {

    /** New System */
    export class BuilderSystem extends ut.ComponentSystem {
        
        OnUpdate():void
        {
            let gameState = this.world.getConfigData(GameState);
            let builderEntity = this.world.getEntityByName("Player");
            let canInteract = false;
            this.world.usingComponentData(builderEntity, [BuilderComponent], (builder) => {
                let isOverAnyButtons = false;
                this.world.usingComponentData(builder.ViewModeButton, [ut.UIControls.Toggle, ut.UIControls.MouseInteraction],
                    (toggle, mouseInteraction) =>
                    {
                        if (ut.Core2D.Input.getKeyDown(ut.Core2D.KeyCode.LeftShift))
                        {
                            toggle.isOn = !toggle.isOn;
                        }
                        if (toggle.isOn != gameState.ViewscrollMode)
                        {
                            this.ResetBuilderState(builder);
                            gameState.ViewscrollMode = toggle.isOn;
                            this.world.setConfigData(gameState);
                        }
                        if (mouseInteraction.over)
                        {
                            isOverAnyButtons = true;
                        }
                    });
                this.world.usingComponentData(builder.BuildModeButton, [ut.UIControls.Toggle, ut.UIControls.MouseInteraction],
                    (toggle, mouseInteraction) =>
                    {
                        if (ut.Core2D.Input.getKeyDown(ut.Core2D.KeyCode.Space))
                        {
                            toggle.isOn = !toggle.isOn;
                        }
                        if (toggle.isOn && gameState.State == GameStateEnum.Running)
                        {
                            gameState.State = GameStateEnum.BuildMode;
                            this.world.setConfigData(gameState);
                        }
                        else if (!toggle.isOn && gameState.State == GameStateEnum.BuildMode)
                        {
                            this.ResetBuilderState(builder);
                            gameState.State = GameStateEnum.Running;
                            this.world.setConfigData(gameState);
                        }
                        if (mouseInteraction.over)
                        {
                            isOverAnyButtons = true;
                        }
                        if (gameState.State == GameStateEnum.BuildMode && !isOverAnyButtons && !gameState.ViewscrollMode)
                        {
                            canInteract = true;
                        }
                    });
            
                this.world.usingComponentData(builder.StatusText, [ut.Text.Text2DRenderer],
                    (textRenderer) =>
                    {
                        if (gameState.ViewscrollMode)
                        {
                            textRenderer.text = "Drag to look around";
                        }
                        else if (gameState.State == GameStateEnum.Running)
                        {
                            textRenderer.text = "";
                        }
                        else
                        {
                            textRenderer.text = "Drag to conjure planks";
                        }
                    });
            });

            if (!canInteract)
            {
                return;
            }

            this.world.usingComponentData(builderEntity, [BuilderComponent], (builder) => {


                let mousePos = Utils.GetPointerWorldPosition(this.world, builder.CameraEntity);
                if (ut.Core2D.Input.getMouseButtonDown(0))
                {
                    //let mousePos = ut.Core2D.Input.getWorldInputPosition(this.world);
                    let pointA = null;

                    let closestSoftPoint = this.FindClosestSoftPoint(mousePos);

                    if (closestSoftPoint != null)
                    {
                        builder.DragStartIsNew = false;
                        pointA = closestSoftPoint;
                    }
                    else
                    {
                        builder.DragStartIsNew = true;
                        pointA = this.SpawnSoftPoint(mousePos);
                    }
                    builder.DragStartPoint = new ut.Entity(pointA.index, pointA.version);
                    console.log("Builder dragstartpoint = ");
                    console.log(builder.DragStartPoint);
                }
                
                let pointA = builder.DragStartPoint;
                if (pointA != null && !pointA.isNone())
                {
                    //let mousePos = ut.Core2D.Input.getWorldInputPosition(this.world);
                    let endPointPosition = mousePos.clone();
                    let closestSoftPoint = this.FindClosestSoftPoint(mousePos);
                    let endIsFixed = false;
                    if (closestSoftPoint != null)
                    {
                        let closestPointPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(closestSoftPoint, ut.Core2D.TransformLocalPosition);
                        endPointPosition = closestPointPos.position.clone();
                        let closestSoftPointComponent = this.world.getComponentData<SoftPointComponent>(closestSoftPoint, SoftPointComponent);
                        endIsFixed = closestSoftPointComponent.Fixed;
                    }
                    let pointAPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(pointA, ut.Core2D.TransformLocalPosition);
                    let maxLinkLength = 3.0;
                    let aToB = endPointPosition.clone().sub(pointAPos.position.clone());
                    let aToBDistance = aToB.length();
                    let linkValid = true;
                    if (aToBDistance > maxLinkLength)
                    {
                        if (closestSoftPoint != null)
                        {
                            linkValid = false;
                        }
                        else
                        {
                            endPointPosition = pointAPos.position.clone().add(aToB.clone().divideScalar(aToBDistance).multiplyScalar(maxLinkLength));
                        }
                    }
                    let softPointHitbox = this.world.getComponentData<ut.HitBox2D.RectHitBox2D>(pointA, ut.HitBox2D.RectHitBox2D);
                    if (builder.DragStartIsNew && Utils.BoxOverlapsTilemap(this.world, Utils.ToVector2(pointAPos.position), softPointHitbox))
                    {
                        linkValid = false;
                    }
                    else if (!endIsFixed && Utils.BoxOverlapsTilemap(this.world, Utils.ToVector2(endPointPosition), softPointHitbox))
                    {
                        linkValid = false;
                    }
                    
                    this.world.usingComponentData(builder.LinePreview,
                        [ut.Core2D.TransformLocalPosition, ut.Core2D.TransformLocalRotation, ut.Core2D.TransformLocalScale, ut.Core2D.Sprite2DRenderer],
                        (pos, rot, scale, renderer) =>
                        {
                            Utils.StretchFromAToB(pos, rot, scale, pointAPos.position.clone(), endPointPosition);
                            if (linkValid)
                            {
                                renderer.color = new ut.Core2D.Color(0, 1, 0, 1);
                            }
                            else
                            {
                                renderer.color = new ut.Core2D.Color(1, 0, 0, 1);
                            }
                        });
                    
                    if (ut.Core2D.Input.getMouseButtonUp(0))
                    {
                        if (linkValid)
                        {
                            let pointB : ut.Entity = null;
                            if (closestSoftPoint != null)
                            {
                                pointB = closestSoftPoint;
                            }
                            else
                            {
                                let newPoint = this.SpawnSoftPoint(endPointPosition);
                                pointB = newPoint;
                            }
                            console.log("PointA =");
                            console.log(pointA);
                            console.log("PointB = ");
                            console.log(pointB);
                            console.log("PointANone = ");
                            console.log(pointA.isNone());
                            console.log("PointBNone = ");
                            console.log(pointB.isNone());
                            console.log("Same=");
                            console.log(Utils.EntityEquals(pointA, pointB));
                            if (!Utils.EntityEquals(pointA, pointB))
                            {
                                this.SpawnSoftJoint(pointA, pointB);
                            }
                        }
                        else
                        {
                            if (builder.DragStartIsNew)
                            {
                                ut.Core2D.TransformService.destroyTree(this.world, pointA, true);
                            }
                        }
                        builder.DragStartPoint = new ut.Entity(null, null);
                        this.world.usingComponentData(builder.LinePreview, [ut.Core2D.TransformLocalPosition],
                            (pos) =>
                            {
                                pos.position = new Vector3(0.0, 100000, 0.0);
                            });
                    }
                }
            });
        }
        ResetBuilderState(builder : BuilderComponent)
        {
            if (builder.DragStartIsNew && !builder.DragStartPoint.isNone())
            {
                ut.Core2D.TransformService.destroyTree(this.world, builder.DragStartPoint, true);
            }
            builder.DragStartPoint = new ut.Entity(null, null);
            this.world.usingComponentData(builder.LinePreview, [ut.Core2D.TransformLocalPosition],
                (pos) =>
                {
                    pos.position = new Vector3(0.0, 100000, 0.0);
                });
        }
        SpawnSoftPoint(mousePos : Vector3) : ut.Entity
        {
            //let spawnedEntities = entities.game.SoftPoint.load(this.world);
            let spawnedEntity = ut.EntityGroup.instantiate(this.world, "game.SoftPoint")[0];
            let softPoint = this.world.getComponentData(spawnedEntity, SoftPointComponent);
            softPoint.LastPos = Utils.ToVector2(mousePos);
            this.world.setComponentData(spawnedEntity, softPoint);

            let softPointPosition = this.world.getComponentData(spawnedEntity, ut.Core2D.TransformLocalPosition);
            softPointPosition.position = mousePos;
            this.world.setComponentData(spawnedEntity, softPointPosition);

            /*this.world.getComponentData(spawnedEntity,
                [ut.Core2D.TransformLocalPosition, SoftPointComponent],
                (transform, softPoint)=>{
                    transform.position = mousePos;
                    softPoint.LastPos = Utils.ToVector2(mousePos);
                }
                );*/
            return spawnedEntity;
        }
        SpawnSoftJoint(pointA: ut.Entity, pointB: ut.Entity)
        {
            //let spawnedEntities = entities.game.SoftJoint.load(this.world);
            let spawnedEntity = ut.EntityGroup.instantiate(this.world, "game.SoftJoint")[0];
            let pointAPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(pointA, ut.Core2D.TransformLocalPosition);
            let pointBPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(pointB, ut.Core2D.TransformLocalPosition);
            let aToB = pointBPos.position.clone().sub(pointAPos.position);
            this.world.usingComponentData(spawnedEntity,
                [SoftJointComponent],
                (softJoint) =>{
                    softJoint.PointA = pointA;
                    softJoint.PointB = pointB;
                    softJoint.StartLength = aToB.length();
                });
        }
        FindClosestSoftPoint(mousePos : Vector3) : ut.Entity
        {
            let selectRadius = 1.0;
            // Check if we're clicking on an existing softpoint
            let closestSoftPoint = null;
            let closestDistanceSqr = selectRadius * selectRadius;
            this.world.forEach([ut.Entity, SoftPointComponent, ut.Core2D.TransformLocalPosition],
                (entity, softPoint, transformPosition) =>
                {
                    let fromMouse = transformPosition.position.sub(mousePos);
                    let distSqr = fromMouse.x * fromMouse.x + fromMouse.y * fromMouse.y;
                    if (distSqr < closestDistanceSqr)
                    {
                        closestDistanceSqr = distSqr;
                        closestSoftPoint = new ut.Entity(entity.index, entity.version);
                    }
                });
                return closestSoftPoint;
        }
    }
}
