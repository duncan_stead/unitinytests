
namespace game {

    /** New System */
    export class PlayerMovementSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            let gameState = this.world.getConfigData(GameState);
            let dt = this.scheduler.deltaTime();

            this.world.forEach([ut.Entity, PlayerCharacter, ut.HitBox2D.RectHitBox2D, ut.Core2D.TransformLocalPosition, ut.Core2D.TransformLocalScale, ut.Core2D.Sprite2DSequencePlayer],
                (entity, player, hitBox, transformPosition, transformScale, sequencePlayer) => {

                if (gameState.State != GameStateEnum.Running || gameState.ViewscrollMode)
                {
                    sequencePlayer.speed = 0;
                    this.world.usingComponentData(player.LeftButton, [ut.Core2D.Sprite2DRenderer], (buttonSpriteRenderer)=>
                    {
                        buttonSpriteRenderer.color = new ut.Core2D.Color(1.0, 1.0, 1.0, 0.0);
                    });
                    this.world.usingComponentData(player.RightButton, [ut.Core2D.Sprite2DRenderer], (buttonSpriteRenderer)=>
                    {
                        buttonSpriteRenderer.color = new ut.Core2D.Color(1.0, 1.0, 1.0, 0.0);
                    });
                    return;
                }
                else
                {
                    this.world.usingComponentData(player.LeftButton, [ut.Core2D.Sprite2DRenderer], (buttonSpriteRenderer)=>
                    {
                        buttonSpriteRenderer.color = new ut.Core2D.Color(1.0, 1.0, 1.0, 1.0);
                    });
                    this.world.usingComponentData(player.RightButton, [ut.Core2D.Sprite2DRenderer], (buttonSpriteRenderer)=>
                    {
                        buttonSpriteRenderer.color = new ut.Core2D.Color(1.0, 1.0, 1.0, 1.0);
                    });
                }
                let movement = new Vector2(0.0, 0.0);
                let leftButton = this.world.getComponentData<ut.UIControls.MouseInteraction>(player.LeftButton, ut.UIControls.MouseInteraction);
                let rightButton = this.world.getComponentData<ut.UIControls.MouseInteraction>(player.RightButton, ut.UIControls.MouseInteraction);
                if (ut.Runtime.Input.getKey(ut.Core2D.KeyCode.D) || ut.Runtime.Input.getKey(ut.Core2D.KeyCode.RightArrow) || rightButton.down)
                {
                    movement.x = 1;
                }
                else if (ut.Runtime.Input.getKey(ut.Core2D.KeyCode.A) || ut.Runtime.Input.getKey(ut.Core2D.KeyCode.LeftArrow) || leftButton.down)
                {
                    movement.x = -1;
                }
                /*if (ut.Runtime.Input.getKey(ut.Core2D.KeyCode.W) || ut.Runtime.Input.getKey(ut.Core2D.KeyCode.UpArrow))
                {
                    movement.y = 1;
                }
                else if (ut.Runtime.Input.getKey(ut.Core2D.KeyCode.S) || ut.Runtime.Input.getKey(ut.Core2D.KeyCode.DownArrow))
                {
                    movement.y = -1;
                }*/
                let scaledMovement : Vector2 = movement.clone().multiplyScalar(player.Speed * dt);
                let newPosition : Vector2 = new Vector2(transformPosition.position.x, transformPosition.position.y);
                newPosition.add(scaledMovement);

                let wouldBeInWall = Utils.BoxOverlapsTilemap(this.world, newPosition, hitBox);
                if (!wouldBeInWall)
                {
                    transformPosition.position = new Vector3(newPosition.x, newPosition.y, 0.0);
                }
                else
                {
                    let mantleSearchStart = newPosition.clone().add(new Vector2(0.0, 1.0));
                    let mantledPosition = Utils.BoxTilemapCollisionSweep(this.world, mantleSearchStart, newPosition.clone(), hitBox);
                    if (mantledPosition.distanceTo(mantleSearchStart) > 0.01)
                    {
                        console.log("Mantling");
                        transformPosition.position = Utils.ToVector3(mantledPosition);
                    }
                }

                let targetSequenceSpeed = Math.abs(movement.x) * 1.75;
                if (sequencePlayer.speed != targetSequenceSpeed)
                {
                    sequencePlayer.speed = targetSequenceSpeed;
                }
                /*let animSpeed = wouldBeInWall ? 0 : 1;
                if (sequencePlayer.speed != animSpeed)
                {
                    sequencePlayer.speed = animSpeed;
                }*/
                let currentScale = transformScale.scale;
                if (movement.x > 0)
                {
                    currentScale.x =  1.0;
                }
                else if (movement.x < 0)
                {
                    currentScale.x = -1.0;
                }
                transformScale.scale = currentScale;

                // Collision
                //let collision = this.world.getComponentData<ut.Tilemap2D.Tilemap>(player.Collision, ut.Tilemap2D.Tilemap);
            });
        }
    }
}
