
namespace game {

    /** New System */
    export class SoftBodySystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            let gameState = this.world.getConfigData(GameState);
            if (gameState.State != GameStateEnum.Running)
            {
                this.world.forEach([SoftJointComponent, ut.Core2D.TransformLocalPosition, ut.Core2D.TransformLocalRotation, ut.Core2D.TransformLocalScale],
                    (softJoint, transformPosition, transformRotation, transformScale) =>
                    {
                        let pointAPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(softJoint.PointA, ut.Core2D.TransformLocalPosition);
                        let pointBPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(softJoint.PointB, ut.Core2D.TransformLocalPosition);
                        Utils.StretchFromAToB(transformPosition, transformRotation, transformScale, pointAPos.position.clone(), pointBPos.position.clone());
                    });

                return;
            }
            let dt = 1.0/60.0;// this.scheduler.deltaTime();

            this.world.forEach([SoftPointComponent, ut.HitBox2D.RectHitBox2D, ut.Core2D.TransformLocalPosition],
                (softPoint, hitBox, transformPosition) =>
                {
                    if (!softPoint.Fixed)
                    {
                        let frictionThisFrame = 0.1 * dt;
                        let position = softPoint.LastPos;
                        let newPosition = Utils.ToVector2(transformPosition.position);
                        newPosition = Utils.BoxTilemapCollisionSweep(this.world, position.clone(), newPosition.clone(), hitBox);
                        
                        let newPositionBeforeAcceleration = newPosition.clone();
                        let scaledAcceleration = softPoint.Acceleration;
                        newPosition = newPosition.clone().multiplyScalar((2.0 - frictionThisFrame)).sub(position.clone().multiplyScalar(1.0 - frictionThisFrame)).add(scaledAcceleration);
                        
                        newPosition = Utils.BoxTilemapCollisionSweep(this.world, position.clone(), newPosition.clone(), hitBox);

                        transformPosition.position = Utils.ToVector3(newPosition);
                        softPoint.LastPos = newPositionBeforeAcceleration;

                        softPoint.Acceleration = new Vector2(0.0, 0.0);
                    }
                });
                this.world.forEach([SoftJointComponent, ut.Core2D.TransformLocalPosition, ut.Core2D.TransformLocalRotation, ut.Core2D.TransformLocalScale],
                    (softJoint, transformPosition, transformRotation, transformScale) =>
                    {
                        let pointA = this.world.getComponentData<SoftPointComponent>(softJoint.PointA, SoftPointComponent);
                        let pointAPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(softJoint.PointA, ut.Core2D.TransformLocalPosition);
                        let pointB = this.world.getComponentData<SoftPointComponent>(softJoint.PointB, SoftPointComponent);
                        let pointBPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(softJoint.PointB, ut.Core2D.TransformLocalPosition);

                        // Do length constraint
                        let aToB = pointBPos.position.clone().sub(pointAPos.position);
                        let currentLength = aToB.length();
                        //let sizeProportion = currentLength / softJoint.StartLength;
                        let changeNeeded = (currentLength - softJoint.StartLength) / currentLength;
                        let amountToMoveA = aToB.clone().multiplyScalar(changeNeeded * 0.5);
                        if (!pointA.Fixed)
                        {
                            pointAPos.position.add(amountToMoveA);
                            this.world.setComponentData(softJoint.PointA, pointAPos);
                        }
                        if (!pointB.Fixed)
                        {
                            pointBPos.position.sub(amountToMoveA);
                            this.world.setComponentData(softJoint.PointB, pointBPos);
                        }
                    });
                this.world.forEach([SoftJointComponent, ut.Core2D.TransformLocalPosition, ut.Core2D.TransformLocalRotation, ut.Core2D.TransformLocalScale],
                    (softJoint, transformPosition, transformRotation, transformScale) =>
                    {
                        let pointAPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(softJoint.PointA, ut.Core2D.TransformLocalPosition);
                        let pointBPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(softJoint.PointB, ut.Core2D.TransformLocalPosition);
                        Utils.StretchFromAToB(transformPosition, transformRotation, transformScale, pointAPos.position.clone(), pointBPos.position.clone());
                    });
        }
    }
}
