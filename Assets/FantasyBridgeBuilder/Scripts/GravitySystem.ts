
/// <reference path="PlayerMovementSystem.ts"/>
/// <reference path="SoftBodySystem.ts"/>

namespace game {

    /** New System */
    @ut.executeAfter(game.PlayerMovementSystem)
    @ut.executeAfter(game.SoftBodySystem)
    export class GravitySystem extends ut.ComponentSystem
    {
        
        OnUpdate():void
        {
            let gameState = this.world.getConfigData(GameState);
            let dt = 1.0 / 60.0;//this.scheduler.deltaTime();
            this.world.forEach([GravityComponent, ut.HitBox2D.RectHitBox2D, ut.Core2D.TransformLocalPosition, PlayerCharacter],
            (gravityComponent, hitBox, transformPosition, player) =>
            {
                let onPlatform = false;
                let footIntersectionPoint : Vector2 = new Vector2(0, -100000);
                let legLineTop = Utils.ToVector2(transformPosition.position).add(new Vector2(0.0, -hitBox.box.height * 0.4));
                let legLineBottom = legLineTop.clone().add(new Vector2(0.0, -hitBox.box.height * 0.4));
                let platformPointA : ut.Entity;
                let platformPointB : ut.Entity;
                let hasWalkablePoint = false;
                this.world.forEach([SoftJointComponent], (softJoint) =>
                {
                    let pointAPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(softJoint.PointA, ut.Core2D.TransformLocalPosition);
                    let pointBPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(softJoint.PointB, ut.Core2D.TransformLocalPosition);
                    let intersectionPoint = Utils.GetLineLineIntersection(legLineTop, legLineBottom, Utils.ToVector2(pointAPos.position),  Utils.ToVector2(pointBPos.position));
                    if (intersectionPoint != null)
                    {
                        // Check angle isn't too steep
                        let slopeDirection = pointBPos.position.clone().sub(pointAPos.position.clone()).normalize();
                        let isWalkable = Math.abs(slopeDirection.y) <= 0.707; // 0.707) // 0.707 is cos 45
                        if (isWalkable || !hasWalkablePoint)
                        {
                            if (intersectionPoint.y > footIntersectionPoint.y || (isWalkable && !hasWalkablePoint))
                            {
                                platformPointA = new ut.Entity(softJoint.PointA.index, softJoint.PointA.version);
                                platformPointB = new ut.Entity(softJoint.PointB.index, softJoint.PointB.version);
                                footIntersectionPoint = intersectionPoint.clone();
                                onPlatform = true;
                                if (isWalkable)
                                {
                                    hasWalkablePoint = true;
                                }
                            }
                        }
                    }
                });

                if (onPlatform)
                {
                    let newPosition = new Vector2(transformPosition.position.x, footIntersectionPoint.y + hitBox.box.height * 0.7);
                    transformPosition.position = Utils.ToVector3(newPosition);
                    let pushForce = -0.1;
                    
                    if (gameState.State == GameStateEnum.Running)
                    {
                        let pointAPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(platformPointA, ut.Core2D.TransformLocalPosition);
                        let pointBPos = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(platformPointB, ut.Core2D.TransformLocalPosition);
                        // Push down on the platform
                        let proportion = Utils.GetProportionAlongLine(Utils.ToVector2(pointAPos.position), Utils.ToVector2(pointBPos.position), footIntersectionPoint);
                        let pointA = this.world.getComponentData(platformPointA, SoftPointComponent);
                        let pointB = this.world.getComponentData(platformPointB, SoftPointComponent);
                        pointA.Acceleration = pointA.Acceleration.clone().add(new Vector2(0.0, pushForce * (1.0 - proportion)));
                        pointB.Acceleration = pointB.Acceleration.clone().add(new Vector2(0.0, pushForce * proportion));
                        this.world.setComponentData(platformPointA, pointA);
                        this.world.setComponentData(platformPointB, pointB);
                    }
                }
                else
                {
                    let velocity = new Vector2(0.0, -gravityComponent.Gravity * dt);
                    let newPosition = new Vector2(transformPosition.position.x, transformPosition.position.y).add(velocity);
                    if (!Utils.BoxOverlapsTilemap(this.world, newPosition, hitBox))
                    {
                        transformPosition.position = Utils.ToVector3(newPosition);
                        if (newPosition.y < -20)
                        {
                            transformPosition.position = Utils.ToVector3(player.LastSafePosition);
                        }
                    }
                    else
                    {
                        player.LastSafePosition = Utils.ToVector2(transformPosition.position);
                    }
                }
                
            });

            if (gameState.State != GameStateEnum.Running)
            {
                return;
            }
            this.world.forEach([SoftPointComponent, ut.HitBox2D.RectHitBox2D, ut.Core2D.TransformLocalPosition],
            (softPoint, hitBox, transformPosition) =>
            {
                let acc = softPoint.Acceleration;
                acc = acc.add(new Vector2(0.0, -0.025));
                softPoint.Acceleration = acc;
            });
        }
    }
}
