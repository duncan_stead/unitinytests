
namespace game {

    /** New System */
    export class Utils
    {
        static GetPointerWorldPosition(world: ut.World, cameraEntity: ut.Entity): Vector3
        {
            let displayInfo = world.getConfigData(ut.Core2D.DisplayInfo);
            let displaySize = new Vector2(displayInfo.width, displayInfo.height);
            let inputPosition = ut.Runtime.Input.getInputPosition();
            return ut.Core2D.TransformService.windowToWorld(world, cameraEntity, inputPosition, displaySize);
        }
        static BoxOverlapsTilemap(world: ut.World, position: ut.Vector2, hitbox : ut.HitBox2D.RectHitBox2D) : boolean
        {
            let collided = false;
            world.forEach([game.CollisionTiles],
                (collisionTiles) => {
                    if (collisionTiles.cols == 0)
                    {
                        return true;
                    }
                  //ASSUMPTIONS
                  //1. Hitbox is not larger than one tile (so we just need to check if each corner of the hitbox is touching a wall
                  let tileMapBottomLeft = collisionTiles.BottomLeft;
  
                  //Get world coordinates of sprite's four corners
                  let corners = [];
                  //Bottom left
                  corners[0] = new Vector2(position.x - (hitbox.box.width * 0.5), position.y - (hitbox.box.height * 0.5));
                  corners[0] = corners[0].sub(tileMapBottomLeft);
                  //Bottom right
                  corners[1] = new Vector2(hitbox.box.width,0).add(corners[0]);
                  //Top left
                  corners[2] = new Vector2(0, hitbox.box.height).add(corners[0]);
                  //Top Right
                  corners[3] = new Vector2(hitbox.box.width, hitbox.box.height).add(corners[0]);
  
                  //Find the tiles corresponding to the four corners
                  for(let i = 0; i < corners.length; i++)
                  {
                    let corner = corners[i];
  
                    //Tilemap array lists tiles left to right, row by row, from the bottom up
                    if (corner.x <= 0 || corner.y <= 0 || corner.x >= collisionTiles.cols || corner.y >= collisionTiles.rows)
                    {
                        continue;
                    }
                    let tilemapIndex = Math.floor(corner.y) * collisionTiles.cols + Math.floor(corner.x);
                    let tileHere = collisionTiles.Grid[tilemapIndex];
                    if (tileHere)
                    {
                        collided = true;
                        return;
                    }
                    else
                    {
                        collided = false;
                        return;
                    }
                  }              
              });
            return collided;
        }
        static BoxTilemapCollisionSweep(world: ut.World, position: ut.Vector2, nextPosition: ut.Vector2, hitbox : ut.HitBox2D.RectHitBox2D): Vector2
        {
            let movementVector = nextPosition.clone().sub(position);
            world.forEach([game.CollisionTiles],
                (collisionTiles) => {
                    if (collisionTiles.cols == 0)
                    {
                        return true;
                    }
                    let tileMapBottomLeft = collisionTiles.BottomLeft;

                    //Get tilemap space coordinates of sprite's four corners
                    let corners = [];
                    //Bottom left
                    corners[0] = new Vector2(position.x - (hitbox.box.width * 0.5), position.y - (hitbox.box.height * 0.5));
                    corners[0] = corners[0].sub(tileMapBottomLeft);
                    //Bottom right
                    corners[1] = new Vector2(hitbox.box.width,0).add(corners[0]);
                    //Top left
                    corners[2] = new Vector2(0, hitbox.box.height).add(corners[0]);
                    //Top Right
                    corners[3] = new Vector2(hitbox.box.width, hitbox.box.height).add(corners[0]);

                    // X movement
                    let frontEdgeX = 0;
                    let xDistanceToNextCellEdge = 0;
                    if (movementVector.x > 0)
                    {
                        frontEdgeX = corners[3].x;
                        xDistanceToNextCellEdge = 1.0 - (frontEdgeX - Math.floor(frontEdgeX));
                    }
                    else
                    {
                        frontEdgeX = corners[0].x;
                        xDistanceToNextCellEdge = frontEdgeX - Math.floor(frontEdgeX);
                    }
                    let xscanMinY = Math.floor(corners[0].y);
                    let xscanMaxY = Math.floor(corners[3].y);
                    let xscanStart = Math.floor(frontEdgeX);
                    let xscanEnd = Math.floor(frontEdgeX + movementVector.x);
                    let xscanDir = 1;
                    if (xscanEnd < xscanStart)
                    {
                        xscanDir = -1;
                    }
                    let xStepsTillWall = 0;
                    let xDistanceCanMove = 0;
                    for (let gridX = xscanStart; gridX != xscanEnd + xscanDir; gridX += xscanDir)
                    {
                        let scanCollision = false;
                        for (let gridY = xscanMinY; gridY <= xscanMaxY; gridY++)
                        {
                            let tilemapIndex = gridY * collisionTiles.cols + gridX;
                            let tileHere = collisionTiles.Grid[tilemapIndex];
                            if (tileHere)
                            {
                                scanCollision = true;
                                break;
                            }
                        }
                        if (scanCollision)
                        {
                            break;
                        }
                        xStepsTillWall = xStepsTillWall + 1;
                        if (gridX == xscanStart)
                        {
                            xDistanceCanMove = xDistanceToNextCellEdge;
                        }
                        else
                        {
                            xDistanceCanMove = xDistanceCanMove + 1;
                        }
                    }
                    if (xDistanceCanMove <= Math.abs(movementVector.x))
                    {
                        movementVector.x = xscanDir * xDistanceCanMove;
                        nextPosition.x = position.x + movementVector.x;
                    }


                    // Y movement
                    let frontEdgeY = 0;
                    let yDistanceToNextCellEdge = 0;
                    if (movementVector.y > 0)
                    {
                        frontEdgeY = corners[3].y;
                        yDistanceToNextCellEdge = 1.0 - (frontEdgeY - Math.floor(frontEdgeY));
                    }
                    else
                    {
                        frontEdgeY = corners[0].y;
                        yDistanceToNextCellEdge = frontEdgeY - Math.floor(frontEdgeY);
                    }
                    let yscanMinX = Math.floor(corners[0].x);
                    let yscanMaxX = Math.floor(corners[3].x);
                    let yscanStart = Math.floor(frontEdgeY);
                    let yscanEnd = Math.floor(frontEdgeY + movementVector.y);
                    let yscanDir = 1;
                    if (yscanEnd < yscanStart)
                    {
                        yscanDir = -1;
                    }
                    let yStepsTillWall = 0;
                    let yDistanceCanMove = 0;
                    for (let gridY = yscanStart; gridY != yscanEnd + yscanDir; gridY += yscanDir)
                    {
                        let scanCollision = false;
                        for (let gridX = yscanMinX; gridX <= yscanMaxX; gridX++)
                        {
                            let tilemapIndex = gridY * collisionTiles.cols + gridX;
                            let tileHere = collisionTiles.Grid[tilemapIndex];
                            if (tileHere)
                            {
                                scanCollision = true;
                                break;
                            }
                        }
                        if (scanCollision)
                        {
                            break;
                        }
                        yStepsTillWall = yStepsTillWall + 1;
                        if (gridY == yscanStart)
                        {
                            yDistanceCanMove = yDistanceToNextCellEdge;
                        }
                        else
                        {
                            yDistanceCanMove = yDistanceCanMove + 1;
                        }
                    }
                    if (yDistanceCanMove <= Math.abs(movementVector.y))
                    {
                        movementVector.y = yscanDir * yDistanceCanMove;
                        nextPosition.y = position.y + movementVector.y;
                    }
              });
              return nextPosition;
        }
        /*static GetLineAABBIntersection(boxPosition : Vector2, hitbox : ut.HitBox2D.RectHitBox2D, posA : Vector2, posB : Vector2) : Vector2
        {

        }*/
        static GetLineLineIntersection(lineAStart: Vector2, lineAEnd: Vector2, lineBStart: Vector2, lineBEnd: Vector2) : Vector2
        {
            let s1 = lineAEnd.clone().sub(lineAStart);
            let s2 = lineBEnd.clone().sub(lineBStart);
            let s = (-s1.y * (lineAStart.x - lineBStart.x) + s1.x * (lineAStart.y - lineBStart.y)) / (-s2.x * s1.y + s1.x * s2.y);
            let t = ( s2.x * (lineAStart.y - lineBStart.y) - s2.y * (lineAStart.x - lineBStart.x)) / (-s2.x * s1.y + s1.x * s2.y);

            if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
            {
                // Collision detected
                return lineAStart.clone().add(s1.multiplyScalar(t));
            }

            return null;
        }
        static GetProportionAlongLine(a: Vector2, b: Vector2, pos: Vector2)
        {
            let fromAToPos = pos.clone().sub(a);
            let fromAToB = b.clone().sub(a);
            return fromAToPos.dot(fromAToB) / fromAToB.lengthSq();
        }

        static ToVector2(v : Vector3) : Vector2
        {
            return new Vector2(v.x, v.y);
        }
        static ToVector3(v : Vector2) : Vector3
        {
            return new Vector3(v.x, v.y, 0.0);
        }

        static StretchFromAToB(position: ut.Core2D.TransformLocalPosition, rotation: ut.Core2D.TransformLocalRotation, scale: ut.Core2D.TransformLocalScale, pointA: Vector3, pointB: Vector3)
        {
            if (pointA.equals(pointB))
            {
                //console.log("A == B!");
                return;
            }
            let aToB = pointB.clone().sub(pointA);
            let radianDirection = Math.atan2(aToB.y, aToB.x);
            //console.log("RadianDirection=");
            //console.log(radianDirection);
            let eulerRotation = new Euler(0.0, 0.0, radianDirection);
            rotation.rotation = new Quaternion().setFromEuler(eulerRotation);

            position.position = pointA.add(pointB).multiplyScalar(0.5);
            scale.scale = new Vector3(aToB.length(), scale.scale.y, scale.scale.z);
        }
        static EntityEquals(a: ut.Entity, b: ut.Entity) : boolean {
            return (a.index == b.index && a.version == b.version);
          }
    }
}
