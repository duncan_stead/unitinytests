namespace game {

    /** New System */
    @ut.executeBefore(game.PlayerMovementSystem)
    export class TileCollisionSystem extends ut.ComponentSystem
    {
        OnUpdate():void
        {
            this.world.forEach([ut.Tilemap2D.Tilemap, game.CollisionTiles],
                (tilemap, collisionTiles) =>
                {
                    if (collisionTiles.cols == 0)
                    {
                        let tileCount = tilemap.tiles.length;
                        let minX = 1000000;
                        let maxX = -100000;
                        let minY = 1000000;
                        let maxY = -100000;
                        for (let i = 0; i < tileCount; i++)
                        {
                            let tilePos = tilemap.tiles[i].position;
                            if (tilePos.x < minX)
                            {
                                minX = tilePos.x;
                            }
                            if (tilePos.x > maxX)
                            {
                                maxX = tilePos.x;
                            }
                            if (tilePos.y < minY)
                            {
                                minY = tilePos.y;
                            }
                            if (tilePos.y > maxY)
                            {
                                maxY = tilePos.y;
                            }
                        }
                        console.log("minX = ");
                        console.log(minX);
                        console.log("maxX = ");
                        console.log(maxX);
                        console.log("minY = ");
                        console.log(minY);
                        console.log("maxY = ");
                        console.log(maxY);
                        let rows = Math.round(maxY - minY) + 1;
                        let columns = Math.round(maxX - minX) + 1;

                        let totalGridSpaces = rows * columns;
                        console.log("TotalGridSpaces = ");
                        console.log(totalGridSpaces);
                        let newGrid: boolean[] = [];
                        for (let i = 0; i < totalGridSpaces; i++)
                        {
                            newGrid[i] = false;
                        }
                        for (let i = 0; i < tileCount; i++)
                        {
                            let tilePos = tilemap.tiles[i].position;
                            let gridX = Math.round(tilePos.x - minX);
                            let gridY = Math.round(tilePos.y - minY);
                            let gridIndex = gridX + gridY * columns;
                            newGrid[gridIndex] = true;
                        }
                        collisionTiles.Grid = newGrid;
                        console.log("GridCount=");
                        console.log(collisionTiles.Grid.length);

                        console.log("Tile Count = ");
                        console.log(tilemap.tiles.length);
                        collisionTiles.rows = rows;
                        collisionTiles.cols = columns;
                        collisionTiles.BottomLeft = new Vector2(minX, minY);
                        console.log("Rows:");
                        console.log(rows);
                        console.log("Columns:");
                        console.log(columns);
                    }
                });
        }
    }
}
