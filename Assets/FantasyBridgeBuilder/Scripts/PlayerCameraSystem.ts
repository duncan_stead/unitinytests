
/// <reference path="PlayerMovementSystem.ts"/>

namespace game {

    /** New System */
    @ut.executeAfter(game.PlayerMovementSystem)
    export class PlayerCameraSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            let dt = this.scheduler.deltaTime();
            let gameState = this.world.getConfigData(GameState);
            let displayInfo = this.world.getConfigData(ut.Core2D.DisplayInfo);

            this.world.forEach([ut.Entity, PlayerCamera, ut.Core2D.Camera2D, ut.Core2D.TransformLocalPosition],
                (entity, playerCam, camera, transform) => {

                let zoom = 4.0;
                let pixelsPerUnit = 16.0;
                let gridSize = 1.0 / pixelsPerUnit;

                let mousePos = Utils.ToVector2(Utils.GetPointerWorldPosition(this.world, entity)).clone().multiplyScalar(pixelsPerUnit).round().multiplyScalar(gridSize);

                if (gameState.ViewscrollMode)
                {
                    if (ut.Core2D.Input.getMouseButtonDown(0))
                    {
                        playerCam.LastCursorWorldPos = mousePos;
                    }
                    if (ut.Core2D.Input.getMouseButton(0))
                    {
                        let toLastWorldPos = playerCam.LastCursorWorldPos.clone().sub(mousePos);
                        playerCam.TrackOffset = playerCam.TrackOffset.clone().add(toLastWorldPos.multiplyScalar(1.0));
                    }
                }
                else if (gameState.State == GameStateEnum.Running)
                {
                    playerCam.TrackOffset = playerCam.TrackOffset.clone().multiplyScalar(0.5);
                }

                let playerTransform = this.world.getComponentData<ut.Core2D.TransformLocalPosition>(playerCam.Player, ut.Core2D.TransformLocalPosition);
                transform.position = playerTransform.position.clone().add(Utils.ToVector3(playerCam.TrackOffset));


                let pixelsWide = displayInfo.width / zoom;
                let pixelsHigh = displayInfo.height / zoom;
                let unitsWide = pixelsWide * gridSize;
                let unitsHigh = pixelsHigh * gridSize;

                let cornerWorldSpace = transform.position.sub(new Vector3(unitsWide * 0.5, unitsHigh * 0.5, 0.0));
                let cornerPixels = cornerWorldSpace.multiplyScalar(pixelsPerUnit);
                cornerPixels = cornerPixels.round();
                cornerWorldSpace = cornerPixels.multiplyScalar(gridSize); // Now rounded to nearest pixel
                let centerWorldSpace = cornerWorldSpace.add(new Vector3(unitsWide * 0.5, unitsHigh * 0.5, 0.0));
                transform.position = centerWorldSpace;


                let unitsY = (displayInfo.height * 0.5 * gridSize) / zoom;
                if (camera.halfVerticalSize != unitsY)
                {
                    camera.halfVerticalSize = unitsY;
                }
            });
        }
    }
}
