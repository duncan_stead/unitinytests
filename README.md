# UniTinyTests

Unity project tiny examples and test projects

Contains FantasyBridgeBuilder game.

The interesting bit is probably Scripts/TileCollisionSystem.ts and SoftBodySystem.ts

Unity editor version 2018.3.4f1, with Project tiny version 0.13.4-preview

A build of the game can be found here:
http://www.gaminggarrison.com/uTiny/

It would be cool to make bitbucket host the build itself, hmm... I'd need to create a new account without the underscore, and having the static page repo public would mean anyone could change the public site...